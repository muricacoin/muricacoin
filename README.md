
# MuricaCoin [FREE]
http://muricaco.in/

![MuricaCoin](http://i.imgur.com/Nn4OLj9.png)

## What is MuricaCoin?
Murica Coin (FREE) is a Scrypt-based PoW/KGW coin with a focus on Freedom and values that mean something. 

## License
CorgiCoin is released under the MIT license. See [COPYING](COPYING)
for more information.

## Block Info

Algorithm: Scrypt PoW/KGW

Block Time: 60 seconds

Difficulty Refactor: every block

Max Coins: 177,600,000

Reward:

Block 1-100: 1776 MuricaCoins (177,600)

Block 101-5,000: 17760 MuricaCoins (87,024,000)

Block 5,001+:  1776 MuricaCoins
